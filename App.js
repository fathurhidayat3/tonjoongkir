// @flow

import React from 'react';
import {WebView} from 'react-native-webview';

const App: () => React$Node = () => {
  return (
    <WebView
      source={{uri: 'https://pluginongkoskirim.com/cek-tarif-ongkir/'}}
    />
  );
};

export default App;
